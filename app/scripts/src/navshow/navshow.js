
window.onscroll = function() {myFunction()};

function myFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("header").classList.add('show');
    }
    else if (document.body.scrollTop < 50 || document.documentElement.scrollTop < 50) {
        document.getElementById("header").classList.remove('show');

    }
}

$(document).ready(function(){
    $(".fadeIn").click(function(){
        $("div.row2").fadeIn();
        $("button.fadeIn").hide();
    });
});